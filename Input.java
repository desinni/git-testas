import java.util.Scanner;

public class Input {
	
	public String scanInput(String question){
		Scanner scan = new Scanner(System.in);
		System.out.println(question);
		String input = scan.nextLine();
		scan.close();
		return input;
	}
}
